﻿using System;

namespace Chess
{
    class Program
    {
        private static readonly int boardSize = 8;
        static void Main(string[] args)
        {
            bool isCheckMate = false;
            Case[,] board = new Case[boardSize, boardSize];
            Round tour = new Round();
            for (int i = 0; i < boardSize * boardSize; i++)
            {
                int v = i / boardSize;
                board[v, i - (boardSize * v)] = new Case(v, i - (boardSize * v));
            }

            // filling the chess board.

            for (int i = 2; i < 6; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    board[j, i].Piece = new Empty(board[j, i]);
                    board[j, i].Piece = new Empty(board[j, i]);
                }
            }
            for (int j = 0; j < boardSize; j++)
            {
                board[j, 1].Piece = new Pawn(board[j, 1], Color.White);
                board[j, 6].Piece = new Pawn(board[j, 6], Color.Black);
            }
            board[0, 0].Piece = new Rook(board[0, 0], Color.White);
            board[7, 0].Piece = new Rook(board[7, 0], Color.White);
            board[0, 7].Piece = new Rook(board[0, 7], Color.Black);
            board[7, 7].Piece = new Rook(board[7, 7], Color.Black);
            board[1, 0].Piece = new Knight(board[1, 0], Color.White);
            board[6, 0].Piece = new Knight(board[6, 0], Color.White);
            board[1, 7].Piece = new Knight(board[1, 7], Color.Black);
            board[6, 7].Piece = new Knight(board[6, 7], Color.Black);
            board[2, 0].Piece = new Bishop(board[2, 0], Color.White);
            board[5, 0].Piece = new Bishop(board[5, 0], Color.White);
            board[2, 7].Piece = new Bishop(board[2, 7], Color.Black);
            board[5, 7].Piece = new Bishop(board[5, 7], Color.Black);
            board[3, 0].Piece = new Queen(board[3, 0], Color.White);
            board[4, 0].Piece = new King(board[4, 0], Color.White);
            board[3, 7].Piece = new Queen(board[3, 7], Color.Black);
            board[4, 7].Piece = new King(board[4, 7], Color.Black);

            while (isCheckMate == false)
            {
                DrawBoard(board);
                board = GiveYourMove(tour, board);
                // Is the ennemy king in check?
                bool inCheck = false;
                for (int i = 0; i < boardSize; i++)
                {
                    for (int j = 0; j < boardSize; j++)
                    {
                        inCheck |= (board[i, j].Piece.Symbol == "♔"
                            && board[i, j].Piece.Couleur != tour.Couleur
                            && board[i, j].Piece.IsInCheck(board));
                    }
                }
                if (inCheck == true)
                {
                    Console.Write("Échec au roi.\n");
                }
                // Next turn
                tour = NewTurn(tour, board);
            }

            Round NewTurn(Round round, Case[,] chessBoard)
            {
                round.Nbr++;
                if (round.Couleur == Color.White)
                    round.Couleur = Color.Black;
                else if (round.Couleur == Color.Black)
                    round.Couleur = Color.White;
                foreach (Case c in chessBoard)
                {
                    c.Piece.PriseEnpassant = false;
                    if (c.Piece.Couleur == round.Couleur)
                    {
                        c.Piece.Enpassant = false;
                    }
                }
                return round;
            }


            Case[,] GiveYourMove(Round round, Case[,] chessBoard)
            {
                string displacement;
                int startLine = -1;
                int arrivalLine = -1;
                int startColumn = -1;
                int arrivalColumn = -1;

                TestRoque result = new TestRoque();
                bool validCases = false;
                bool validMove = false;
                bool validColor = false;
                bool validOrder = false;
                // lire la console
                while (validOrder == false)
                {
                    Console.Write("{0} turn, give your movement (line,column)" +
                        "->(line,column):", tour.Couleur);
                    displacement = Console.ReadLine();
                    // exit the loop.
                    if (displacement == "break")
                        goto Problem;
                    // castling short code
                    else if (displacement == "petit roque")
                    {
                        result = Roque(chessBoard, false, round);
                        if (result.CanRoque)
                            return result.ChessBoard;
                        else
                            continue;
                    }
                    // castling long code
                    else if (displacement == "grand roque")
                    {
                        result = Roque(chessBoard, true, round);
                        if (result.CanRoque)
                            return result.ChessBoard;
                        else
                            continue;
                    }
                    // other moves
                    else if (displacement.Length == 12)
                    {
                        // read the beginning and ending positions
                        startLine = displacement[1];
                        startLine -= 49;
                        startColumn = displacement[3];
                        startColumn -= 49;
                        arrivalLine = displacement[8];
                        arrivalLine -= 49;
                        arrivalColumn = displacement[10];
                        arrivalColumn -= 49;
                        bool v1 = (Validité(startLine) && Validité(arrivalLine)
                                && Validité(startColumn) && Validité(arrivalColumn));
                        if (v1 == false)
                        {
                            Console.Write("lines and columns are integers " +
                                    "between 1 and 8.\n");
                            continue;
                        }
                        else
                            validCases = true;
                    }
                    else
                    {
                        Console.Write("Respect formating.\n");
                        continue;
                    }
                    // The piece in the beginning position is the same color 
                    //as the turn played.
                    validColor = false || chessBoard[startColumn,
                        startLine].Piece.Couleur == round.Couleur;
                    if (!validColor)
                    {
                        Console.Write("Don't move your opponent pieces.\n");
                        continue;
                    }
                    // The move is a valid move for the chosed piece.
                    validMove = chessBoard[startColumn, startLine].Piece.
                        CanIMoveThere(chessBoard[arrivalColumn, arrivalLine], chessBoard);
                    if (!validMove)
                    {
                        Console.Write("Respect chess rules!\n");
                        continue;
                    }
                    // If everything else is valid, we check for our king being 
                    //in check at the end of the move.

                    // En passant catch.
                    if (validMove && validColor && validCases
                        && chessBoard[startColumn, startLine].Piece.PriseEnpassant)
                    {
                        chessBoard[startColumn, startLine].Piece.
                                MovePiece(chessBoard[arrivalColumn, arrivalLine], chessBoard);
                        chessBoard[arrivalColumn, startLine].Piece
                           = new Empty(chessBoard[arrivalColumn, startLine]);
                        bool isInCheck = false;
                        for (int i = 0; i < boardSize; i++)
                        {
                            for (int j = 0; j < boardSize; j++)
                            {
                                isInCheck |= (chessBoard[i, j].Piece.Symbol == "♔"
                                    && chessBoard[i, j].Piece.Couleur == round.Couleur
                                    && chessBoard[i, j].Piece.IsInCheck(chessBoard));
                            }
                        }
                        if (isInCheck)
                        {
                            Console.Write("This move ends with the king in " +
                                "check.\n");
                            chessBoard[arrivalColumn, arrivalLine].Piece.
                                MovePiece(chessBoard[startColumn, startLine], chessBoard);
                            if (tour.Couleur == Color.White)
                            {
                                chessBoard[arrivalColumn, startLine].Piece
                                = new Pawn(chessBoard[arrivalColumn, startLine]
                                 , Color.Black)
                                {
                                    HasMoved = true,
                                    Enpassant = true
                                };
                            }
                            else
                            {
                                chessBoard[arrivalColumn, startLine].Piece
                                 = new Pawn(chessBoard[arrivalColumn, startLine]
                                 , Color.White)
                                 {
                                     HasMoved = true,
                                     Enpassant = true
                                 };
                            }
                            continue;
                        }
                        return chessBoard;
                    }
                    // Other moves.
                    if (validMove && validColor && validCases)
                    {
                        bool depmem = chessBoard[startColumn,
                            startLine].Piece.HasMoved;
                        chessBoard[startColumn, startLine].Piece.MovePiece
                            (chessBoard[arrivalColumn, arrivalLine], chessBoard);
                        // Check that the king is not in check.
                        bool isInCheck = false;
                        for (int i = 0; i < boardSize; i++)
                        {
                            for (int j = 0; j < boardSize; j++)
                            {
                                isInCheck |= (chessBoard[i, j].Piece.Symbol == "♔"
                                    && chessBoard[i, j].Piece.Couleur == round.Couleur
                                    && chessBoard[i, j].Piece.IsInCheck(chessBoard));
                            }
                        }
                        if (isInCheck)
                        {
                            Console.Write("This move ends with the king in " +
                                "check.\n");
                            chessBoard[arrivalColumn, arrivalLine].Piece.Enpassant
                                 = false;
                            chessBoard[arrivalColumn, arrivalLine].Piece.
                                MovePiece(chessBoard[startColumn, startLine], chessBoard);
                            chessBoard[startColumn, startLine].Piece.HasMoved = depmem;
                            continue;
                        }
                        //promotion
                        chessBoard[arrivalColumn, arrivalLine].
                                Piece.Promotion(chessBoard);
                        return chessBoard;
                    }
                    else
                        continue;
                }
            Problem:
                Console.Write("A problem occurs.\n");
                return chessBoard;
            }
            // Check that ligns and columns are in the good interval.
            bool Validité(int i)
            {
                if (i > -1 && i < boardSize)
                    return true;
                else
                    return false;
            }

            TestRoque Roque(Case[,] chessBoard, bool grand, Round round)
            {
                TestRoque result = new TestRoque();
                int i = 0;
                //direction displacement
                int dd = 1;
                // displacement Rook
                int dt = -2;
                // position of the Rook
                int pt = 7;
                // line of the king
                if (round.Couleur == Color.Black)
                    i = 7;
                // Can you castle?
                bool v1 = (!chessBoard[4, i].Piece.HasMoved
                    && !chessBoard[4, i].Piece.IsInCheck(chessBoard)
                    && !chessBoard[pt, i].Piece.HasMoved
                    && chessBoard[pt, i].Piece.Couleur != Color.Rouge
                    && chessBoard[pt - dd, i].Piece.Couleur == Color.Rouge
                    && chessBoard[pt - 2 * dd, i].Piece.Couleur == Color.Rouge);
                if (grand)
                {
                    dd = -1;
                    dt = 3;
                    pt = 0;
                    v1 = v1
                    && chessBoard[pt - 3 * dd, i].Piece.Couleur == Color.Rouge;
                }

                // Is the king in check during castle?
                if (v1)
                {
                    // Implement the move
                    chessBoard[4, i].Piece.MovePiece(chessBoard[4 + dd, i], chessBoard);
                    if (chessBoard[4 + dd, i].Piece.IsInCheck(chessBoard))
                    {
                        // Go back to the first position.
                        Console.Write("You cannot castle, " +
                            "the king is in check.\n");
                        chessBoard[4 + dd, i].Piece.MovePiece(chessBoard[4, i], chessBoard);
                        chessBoard[4, i].Piece.HasMoved = false;
                        result.CanRoque = false;
                        result.ChessBoard = chessBoard;
                        return result;
                    }
                    // Implement the move
                    chessBoard[4 + dd, i].Piece.MovePiece(chessBoard[4 + (2 * dd), i], chessBoard);
                    if (chessBoard[4 + (2 * dd), i].Piece.IsInCheck(chessBoard))
                    {
                        // Go back to the first position.
                        Console.Write("You cannot castle, " +
                            "the king is in check.\n");
                        chessBoard[4 + (2 * dd), i].Piece.MovePiece(chessBoard[4, i], chessBoard);
                        chessBoard[4, i].Piece.HasMoved = false;
                        result.CanRoque = false;
                        result.ChessBoard = chessBoard;
                        return result;
                    }
                    chessBoard[pt, i].Piece.MovePiece(chessBoard[pt + dt, i], chessBoard);
                    result.CanRoque = true;
                    result.ChessBoard = chessBoard;
                    return result;
                }
                Console.Write("You cannot castle.\n");
                return result;
            }

            // Drawing the board.
            void DrawBoard(Case[,] chessBoard)
            {
                Console.Write("\n");
                Console.Write(" ");
                for (int i = 0; i < boardSize; i++)
                {
                    Console.Write("|");
                    Console.Write(i + 1);
                }
                Console.Write("|\n");
                for (int i = 0; i < boardSize; i++)
                {
                    Console.Write(i + 1);
                    for (int j = 0; j < boardSize; j++)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("|");
                        if (chessBoard[j, i].Piece.Couleur == Color.White)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write(chessBoard[j, i].Piece.Symbol);
                        }
                        else if (chessBoard[j, i].Piece.Couleur == Color.Black)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.Write(chessBoard[j, i].Piece.Symbol);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.Write(chessBoard[j, i].Piece.Symbol);
                        }
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.Write("|\n");
                }
            }
        }
    }
    // Define the colors that the pieces can take. 
    // We added Rouge (red) for the empty cases.
    public enum Color
    {
        White,
        Black,
        Rouge
    }

    // During the test for the Castle, we need a boolean and a board.
    public class TestRoque
    {
        public bool CanRoque { get; set; }
        public Case[,] ChessBoard { get; set; }
    }

    // Define the round with color and number.
    public class Round
    {
        public Color Couleur { get; set; }
        public int Nbr { get; set; }

        public Round()
        {
            Couleur = Color.White;
            Nbr = 1;
        }
    }
    // Squares of the chessboard
    public class Case
    {
        public int Column { get; set; }
        public int Line { get; set; }
        public Pièce Piece { get; set; }

        public Case(int colonne, int ligne, Pièce piece1)
        {
            Line = ligne;
            Column = colonne;
            Piece = piece1;
        }

        public Case(int colonne, int ligne)
        {
            Line = ligne;
            Column = colonne;
            Piece = new Empty(colonne, ligne);
        }

        public Case()
        {
            Line = -1;
            Column = -1;
            Piece = null;
        }

    }

    // Different king of pieces.
    public abstract class Pièce
    {
        private static readonly int boardSize = 8;

        public int Line { get; set; }
        public int Colonne { get; set; }
        public Color Couleur { get; set; }

        public string Symbol;
        public bool HasMoved;
        public bool Enpassant;
        public bool PriseEnpassant;

        protected Pièce(Case casse, Color color)
        {
            Line = casse.Line;
            Colonne = casse.Column;
            Couleur = color;
        }

        protected Pièce(int colonne, int ligne, Color color)
        {
            Line = ligne;
            Colonne = colonne;
            Couleur = color;
        }

        public abstract bool CanIMoveThere(Case Case, Case[,] chessBoard);
        // Move the pieces
        public void MovePiece(Case Case, Case[,] chessBoard)
        {
            Case Temp = chessBoard[Colonne, Line];
            // Change the piece in the arrival square.
            Case.Piece = chessBoard[Colonne, Line].Piece;
            // Remove the previous Piece.
            chessBoard[Colonne, Line].Piece = new Empty(Temp);
            // Change the position inside the piece.
            Case.Piece.Line = Case.Line;
            Case.Piece.Colonne = Case.Column;
            // The piece has moved.
            Case.Piece.HasMoved = true;
        }

        // Check if the squares are empty.
        public bool AreSquaresEmpty(Case CaseBegin, Case CaseEnd, Case[,] chessBoard)
        {
            // direction of the displacement:
            int dc = Math.Sign(CaseBegin.Column - CaseEnd.Column);
            int dl = Math.Sign(CaseBegin.Line - CaseEnd.Line);
            // number of square advanced through:
            int NbrSquare = Math.Max(Math.Abs(CaseBegin.Line - CaseEnd.Line),
                Math.Abs(CaseBegin.Column - CaseEnd.Column));
            // the displacement is a straight line or a diagonal:
            bool v0 = dc * dl == 0 ||
                Math.Abs(CaseBegin.Line - CaseEnd.Line) ==
                Math.Abs(CaseBegin.Column - CaseEnd.Column);
            bool v1 = v0 && NbrSquare != 0 &&
                CaseBegin.Piece.Couleur != CaseEnd.Piece.Couleur;
            if (!v1)
                return false;
            // Are the squares touching?
            if (v1 && NbrSquare == 1)
                return true;
            // Are the visited squares empty?
            for (int j = 1; j < NbrSquare; j++)
            {
                v1 &= chessBoard[CaseBegin.Column + (j * dc),
                    CaseBegin.Line + (j * dl)].Piece.Couleur == Color.Rouge;
            }
            return v1;
        }

        public bool IsPuttingInCheck(Case[,] chessBoard)
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (chessBoard[i, j].Piece.Symbol == "♔"
                        && chessBoard[i, j].Piece.Couleur != Couleur
                        && CanIMoveThere(chessBoard[i, j], chessBoard))
                        return true;
                }
            }
            return false;
        }

        public bool IsInCheck(Case[,] chessBoard)
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (chessBoard[i, j].Piece.Couleur != Couleur
                        && chessBoard[i, j].Piece.CanIMoveThere
                        (chessBoard[Colonne, Line], chessBoard))
                    {
                        Console.Write(chessBoard[i, j].Piece.Symbol);
                        Console.Write(chessBoard[i, j].Piece.Couleur);
                        Console.Write("\n");
                        return true;
                    }
                }
            }
            return false;
        }

        // promotion of the Pawn
        public void Promotion(Case[,] chessBoard)
        {
            string prom;
            if (((Couleur == Color.White && Line == 7)
                || (Couleur == Color.Black && Line == 0))
                && Symbol == "♙")
            {
                Console.Write("Change your Pawn in : Queen(Q), Rook(R), " +
                    "Bishop(B), " + "Knight(C):");
                prom = Console.ReadLine();
                Console.Write(prom + "\n");
                if (prom == "Q")
                {
                    chessBoard[Colonne, Line].Piece = new Queen(Colonne, Line, Couleur);
                    {
                        HasMoved = true;
                    };
                }
                else if (prom == "R")
                {
                    chessBoard[Colonne, Line].Piece = new Rook(Colonne, Line, Couleur);
                    {
                        HasMoved = true;
                    }
                }
                else if (prom == "B")
                {
                    chessBoard[Colonne, Line].Piece = new Bishop(Colonne, Line, Couleur);
                    {
                        HasMoved = true;
                    }
                }
                else if (prom == "C")
                {
                    chessBoard[Colonne, Line].Piece = new Knight(Colonne, Line, Couleur);
                    {
                        HasMoved = true;
                    }
                }
                else
                {
                    Console.Write("Changed to : Queen.");
                    chessBoard[Colonne, Line].Piece = new Queen(Colonne, Line, Couleur);
                    {
                        HasMoved = true;
                    }
                }
            }
        }
    }
    internal class Empty : Pièce
    {
        public Empty(Case casse)
            : base(casse, Color.Rouge)
        {
            Symbol = "_";
        }
        public Empty(int colonne, int ligne)
            : base(colonne, ligne, Color.Rouge)
        {
            Symbol = "_";
        }

        public override bool CanIMoveThere(Case Case, Case[,] chessBoard)
        {
            return false;
        }
    }
    class Pawn : Pièce
    {
        public Pawn(Case casse, Color color)
            : base(casse, color)
        {
            Symbol = "♙";
        }
        public Pawn(int colonne, int ligne, Color color)
            : base(colonne, ligne, color)
        {
            Symbol = "♙";
        }

        public override bool CanIMoveThere(Case Case, Case[,] chessBoard)
        {
            int sign = 1;
            if (Couleur == Color.White)
                sign = -1;
            /*
                          |
                          |
                          ♙
        */
            // Move two cases for the first move.
            if (Line - Case.Line == sign * 2
                && Colonne - Case.Column == 0
                && HasMoved == false
                && Case.Piece.Couleur == Color.Rouge
                && chessBoard[Colonne, Line - sign].Piece.Couleur
                 == Color.Rouge)
            {
                return true;
            }
            /*
                          |
                          ♙
        */
            // Move one case forward.
            else if (Line - Case.Line == sign
                && Colonne - Case.Column == 0
                && Case.Piece.Couleur == Color.Rouge)
            {
                return true;
            }
            /*
                         \ /
                          ♙  only to take a piece.
        */
            // Take in diagonal
            else if (Case.Piece.Couleur != Couleur
                && Case.Piece.Couleur != Color.Rouge
                && Line - Case.Line == sign
                && Math.Abs(Colonne - Case.Column) == 1)
            {
                return true;
            }
            /*
                         ♙ |   ♙ ♟   ♙ ♟    
                           |          \     ♙
                           ♟
        */
            // en passant
            else if (Colonne > 0
                && chessBoard[Colonne - 1, Line].Piece.Enpassant
                && Case.Column == Colonne - 1
                && Line == Case.Line + sign)
            {
                PriseEnpassant = true;
                return true;
            }
            else if (Colonne < 7
                && chessBoard[Colonne + 1, Line].Piece.Enpassant
                && Case.Column == Colonne + 1
                && Line == Case.Line + sign)
            {
                PriseEnpassant = true;
                return true;
            }
            else
            {
                return false;
            }
        }

    }

    class Rook : Pièce
    {
        public Rook(Case casse, Color color)
            : base(casse, color)
        {
            Symbol = "♖";
        }

        public Rook(int colonne, int ligne, Color color)
            : base(colonne, ligne, color)
        {
            Symbol = "♖";
        }
        /*
                          |
                         -♖-  any number of empty squares
                          |
        */
        public override bool CanIMoveThere(Case Case, Case[,] chessBoard)
        {
            return (Line - Case.Line == 0 || Colonne - Case.Column == 0)
                && AreSquaresEmpty(chessBoard[Colonne, Line], Case, chessBoard);
        }
    }

    class Knight : Pièce
    {
        public Knight(Case casse, Color color)
            : base(casse, color)
        {
            /* This can be used to differentiate black and white for 
             * colorblind people.
            if (color == Couleur.White)
                Symbol = "♘";
            else
                Symbol = "♞";
                */
            Symbol = "♘";
        }

        public Knight(int colonne, int ligne, Color color)
            : base(colonne, ligne, color)
        {
            Symbol = "♘";
        }
        /*
                          |-    -|  
                          |      |  |--   --|
                          ♘      ♘  ♘       ♘
        */
        public override bool CanIMoveThere(Case Case, Case[,] chessBoard)
        {
            if (Couleur != Case.Piece.Couleur
                && Math.Max(Math.Abs(Colonne - Case.Column),
                Math.Abs(Line - Case.Line)) == 2
                && Math.Min(Math.Abs(Colonne - Case.Column),
                Math.Abs(Line - Case.Line)) == 1)
                return true;
            else
                return false;
        }
    }

    class Bishop : Pièce
    {
        public Bishop(Case casse, Color color)
            : base(casse, color)
        {
            Symbol = "♗";
        }
        public Bishop(int colonne, int ligne, Color color)
            : base(colonne, ligne, color)
        {
            Symbol = "♗";
        }
        /*
                        \ /
                         ♗  any number of empty squares
                        / \
        */
        public override bool CanIMoveThere(Case Case, Case[,] chessBoard)
        {
            return Math.Abs(Line - Case.Line) == Math.Abs(Colonne - Case.Column)
                && AreSquaresEmpty(chessBoard[Colonne, Line], Case, chessBoard);
        }
    }

    class Queen : Pièce
    {
        public Queen(Case casse, Color color)
            : base(casse, color)
        {
            Symbol = "♕";
        }

        public Queen(int colonne, int ligne, Color color)
            : base(colonne, ligne, color)
        {
            Symbol = "♕";
        }
        /*
                        \|/
                        -♕- any number of empty squares
                        /|\
        */
        public override bool CanIMoveThere(Case Case, Case[,] chessBoard)
        {
            return AreSquaresEmpty(chessBoard[Colonne, Line], Case, chessBoard);
        }
    }

    class King : Pièce
    {
        public King(Case casse, Color color)
            : base(casse, color)
        {
            Symbol = "♔";
        }

        public King(int colonne, int ligne, Color color)
            : base(colonne, ligne, color)
        {
            Symbol = "♔";
        }
        /*
                        \|/
                        -♔- only one square at a time
                        /|\
        */
        public override bool CanIMoveThere(Case Case, Case[,] chessBoard)
        {
            return Couleur != Case.Piece.Couleur
                && Math.Max(Math.Abs(Colonne - Case.Column),
                Math.Abs(Line - Case.Line)) == 1;
        }
    }
}

