# Chess Game

This a chess game made in c# that plays on the Terminal window. You can play against yourself or another player 
and the program will tell you if the moves are legal.

## Main entries

To move a piece enter the following command when prompted in the Terminal window:
(i_b,j_b)->(i_e,j_e)
where i_b and j_b are respectively the line and the column of the piece you want to move at the beginning 
of your turn, and i_e and j_e are the line and the column you want to move your piece to.
e.g:


```
 |1|2|3|4|5|6|7|8|
1|♖|♘|♗|♕|♔|♗|♘|♖|
2|♙|♙|♙|♙|♙|♙|♙|♙|
3|_|_|_|_|_|_|_|_|
4|_|_|_|_|_|_|_|_|
5|_|_|_|_|_|_|_|_|
6|_|_|_|_|_|_|_|_|
7|♙|♙|♙|♙|♙|♙|♙|♙|
8|♖|♘|♗|♕|♔|♗|♘|♖|
```
White turn, give your movement 
```
(line,column)->(line,column):
```
You want to move your Knight from 1st line second column to the 3rd line, 1st column you type:

```
(line,column)->(line,column):(1,2)->(3,1)
```

And the board changes to:

```
 |1|2|3|4|5|6|7|8|
1|♖|_|♗|♕|♔|♗|♘|♖|
2|♙|♙|♙|♙|♙|♙|♙|♙|
3|♘|_|_|_|_|_|_|_|
4|_|_|_|_|_|_|_|_|
5|_|_|_|_|_|_|_|_|
6|_|_|_|_|_|_|_|_|
7|♙|♙|♙|♙|♙|♙|♙|♙|
8|♖|♘|♗|♕|♔|♗|♘|♖|
```

Black turn, give your movement (line,column)->(line,column):

Should you input a wrong move as: 
```
 |1|2|3|4|5|6|7|8|
1|♖|♘|♗|♕|♔|♗|♘|♖|
2|♙|♙|♙|♙|♙|♙|♙|♙|
3|_|_|_|_|_|_|_|_|
4|_|_|_|_|_|_|_|_|
5|_|_|_|_|_|_|_|_|
6|_|_|_|_|_|_|_|_|
7|♙|♙|♙|♙|♙|♙|♙|♙|
8|♖|♘|♗|♕|♔|♗|♘|♖|

White turn, give your movement (line,column)->(line,column):(1,2)->(3,2)
```
```
 |1|2|3|4|5|6|7|8|
1|♖|♘|♗|♕|♔|♗|♘|♖|
2|♙|♙|♙|♙|♙|♙|♙|♙|
3|_|_|_|_|_|_|_|_|
4|_|_|_|_|_|_|_|_|
5|_|_|_|_|_|_|_|_|
6|_|_|_|_|_|_|_|_|
7|♙|♙|♙|♙|♙|♙|♙|♙|
8|♖|♘|♗|♕|♔|♗|♘|♖|
White turn, give your movement (line,column)->(line,column):(1,2)->(3,2)
Respect chess rules!
White turn, give your movement (line,column)->(line,column):
```
As you can see the program says that the move is invalid, and you need to enter a new move.


---

## Some additionnal entries

If you want to castle, rather than entering the displacement of a piece, type:
"petit roque" for a small castle
"grand roque" for a big castle.

Respect the formating, no additional spaces or extra symbols.

